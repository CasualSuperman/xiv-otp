package main

import (
	"context"
	"github.com/labstack/gommon/log"
	"github.com/wailsapp/wails/v2/pkg/runtime"
	"strings"
	"xiv-otp/config"
	"xiv-otp/launcher"
	"xiv-otp/otp"
)

// App struct
type App struct {
	ctx             context.Context
	msg             string
	config          config.Config
	otpRequest      otp.OTPSender
	launcherStarted bool
}

// NewApp creates a new App application struct
func NewApp() *App {
	cfg, err := config.ReadConfig()
	msg := "Config Loaded"
	if err != nil {
		msg = "Config Failed to Load: " + err.Error()
	}
	return &App{msg: msg, config: cfg}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
	runtime.EventsEmit(a.ctx, "onMessage", a.msg)
}

func (a *App) IsOtpConfigured() bool {
	return a.config.Otp != nil
}

func (a *App) IsXIVLauncherFound() bool {
	return launcher.IsLauncherFound(a.config)
}

func (a *App) setMessage(message string) {
	log.Info(message)
	if a.msg != message {
		a.msg = message
		runtime.EventsEmit(a.ctx, "onMessage", a.msg)
	}
}

func (a *App) GetMessage() string {
	return a.msg
}

func (a *App) UpdateLauncherPath(path string) error {
	if a.config.LauncherPath != nil && *a.config.LauncherPath == path {
		return nil
	}
	a.config.LauncherPath = &path
	return config.WriteConfig(a.config)
}

func (a *App) UpdateOTP(url string) {
	a.setMessage("Updating OTP URL")
	var totpCfg *config.JsonTOTP
	if strings.HasPrefix(url, "otpauth://") {
		cfg, err := config.ParseTotpConfigUri(url)
		if err != nil {
			a.setMessage("Failed to Parse OTP URI: " + err.Error())
			return
		}
		totpCfg = cfg
	} else {
		totpCfg = &config.JsonTOTP{
			Secret:    url,
			Issuer:    "Square Enix ID",
			Algorithm: "SHA1",
			Digits:    6,
			Period:    30,
		}
	}
	if a.config.Otp == nil || *totpCfg != *a.config.Otp {
		a.config.Otp = totpCfg
		a.setMessage("OTP URL updated, saving config")
		a.saveConfig()
		if a.otpRequest != nil {
			a.otpRequest.Cancel()
			a.otpRequest = nil
			a.StartOTPRequests()
		}
	} else {
		a.setMessage("OTP URL was identical")
	}
}

func (a *App) PickLauncherPath() {
	path, err := runtime.OpenFileDialog(a.ctx, runtime.OpenDialogOptions{
		Title:                      "XIVLauncher Executable",
		CanCreateDirectories:       false,
		TreatPackagesAsDirectories: false,
		ShowHiddenFiles:            false,
	})
	if err != nil {
		a.setMessage("Failed to open file chooser")
		return
	}
	if path == "" || launcher.IsDefaultPath(path) {
		a.config.LauncherPath = nil
		a.saveConfig()
	} else if a.config.LauncherPath == nil || *a.config.LauncherPath != path {
		a.config.LauncherPath = &path
		a.saveConfig()
	}
}

func (a *App) saveConfig() {
	err := config.WriteConfig(a.config)
	if err != nil {
		a.setMessage("Failed to save Config: " + err.Error())
	} else {
		a.setMessage("Config saved")
	}
}

func (a *App) StartXIVLauncher() {
	if !a.launcherStarted {
		err := launcher.StartXIVLauncher(a.config)
		if err != nil {
			a.setMessage("Failed to start XIVLauncher: " + err.Error())
		} else {
			a.launcherStarted = true
			a.setMessage("Started XIVLauncher")
		}
	}
}

func (a *App) StartOTPRequests() {
	if a.config.Otp == nil {
		a.setMessage("Cannot start OTP Requests, OTP not configured")
		return
	}
	if a.otpRequest == nil {
		cancel, err := otp.StartOTPSend(a.config, a.ctx, func() {
			a.setMessage("Started XIVLauncher, closing")
			runtime.Quit(a.ctx)
		})
		if err != nil {
			a.setMessage("Failed to start OTP Requests: " + err.Error())
			return
		}
		a.otpRequest = cancel
		a.setMessage("Sending OTP to XIVLauncher...")
	}
}

func (a *App) StopOTPRequests() {
	if a.otpRequest != nil {
		a.otpRequest.Cancel()
		a.otpRequest = nil
	}
}
