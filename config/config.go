package config

import "github.com/tucnak/store"

type Config struct {
	Otp          *JsonTOTP `json:"otp"`
	LauncherPath *string   `json:"launcherPath"`
}

func init() {
	store.Init("xiv-otp")
}

func ReadConfig() (Config, error) {
	cfg := new(Config)
	err := store.Load("config.json", &cfg)
	return *cfg, err
}

func WriteConfig(cfg Config) error {
	return store.Save("config.json", cfg)
}
