package config

import "github.com/pquerna/otp"

type JsonTOTP struct {
	Secret    string `json:"secret"`
	Issuer    string `json:"issuer"`
	Algorithm string `json:"algorithm"`
	Digits    int    `json:"digits,string"`
	Period    uint64 `json:"period,string"`
}

func ParseTotpConfigUri(uri string) (*JsonTOTP, error) {
	key, err := otp.NewKeyFromURL(uri)
	if err != nil {
		return nil, err
	}
	return &JsonTOTP{Secret: key.Secret(), Issuer: key.Issuer(), Algorithm: key.Algorithm().String(), Digits: key.Digits().Length(), Period: key.Period()}, nil
}
