package launcher

import (
	"log"
	"os"
	"os/exec"
	"xiv-otp/config"
)

func StartXIVLauncher(config config.Config) error {
	launcherPath := defaultXivLauncherBin
	if config.LauncherPath != nil && *config.LauncherPath != "" {
		launcherPath = *config.LauncherPath
	}
	cmd := exec.Command(launcherPath)
	return cmd.Start()
}

func IsLauncherFound(config config.Config) bool {
	path := getLauncherPath(config)
	log.Println("Launcher path: " + path)
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func getLauncherPath(config config.Config) string {
	launcherPath := defaultXivLauncherBin
	if config.LauncherPath != nil && *config.LauncherPath != "" {
		launcherPath = *config.LauncherPath
	}
	return launcherPath
}

func IsDefaultPath(path string) bool {
	return path == defaultXivLauncherBin
}

func GetDefaultLauncherDir() string {
	return defaultXivLauncherDir
}

func GetDefaultLauncherFile() string {
	return defaultXivLauncherApp
}
