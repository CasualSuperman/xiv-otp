package launcher

import (
	"os"
)

var defaultXivLauncherDir = os.ExpandEnv("${localappdata}") + string(os.PathSeparator) + "XIVLauncher"
var defaultXivLauncherApp = "XIVLauncher.exe"
var defaultXivLauncherBin = defaultXivLauncherDir + string(os.PathSeparator) + defaultXivLauncherApp
