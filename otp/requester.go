package otp

import (
	"context"
	"log"
	"net"
	"net/http"
	"time"
	"xiv-otp/config"
)

type OTPSender interface {
	context.Context
	Cancel()
}

type ContextCombo struct {
	context.Context
	cancelFunc context.CancelFunc
}

func (c ContextCombo) Cancel() {
	c.cancelFunc()
}

func StartOTPSend(config config.Config, parentCtx context.Context, successListener func()) (OTPSender, error) {
	totp, err := CreateOtp(*config.Otp)
	if err != nil {
		return nil, err
	}

	client := http.Client{
		Transport: &http.Transport{
			Dial: func(network string, addr string) (net.Conn, error) {
				return net.DialTimeout(network, addr, time.Second/10)
			},
		},
	}

	ctx, cancel := context.WithCancel(parentCtx)
	go func() {
		defer cancel()
		defer client.CloseIdleConnections()
		for {
			if makeRequest(client, totp) {
				successListener()
				break
			}
			select {
			case <-time.After(time.Second / 2):
				log.Println("Request failed, trying again...")
			case <-ctx.Done():
				log.Println("Context cancelled, stopping requests")
				return
			}

		}
	}()
	return ContextCombo{ctx, cancel}, nil
}

func makeRequest(client http.Client, totp CurrTotp) bool {
	resp, err := client.Get("http://127.0.0.1:4646/ffxivlauncher/" + totp.Current())
	if err != nil {
		log.Printf("Request failed: %v\n", err)
		return false
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		return true
	}
	return false
}
