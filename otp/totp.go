package otp

import (
	"crypto/sha1"
	"errors"
	"github.com/xlzd/gotp"
	"hash"
	"strings"
	"xiv-otp/config"
)

func CreateOtp(totp config.JsonTOTP) (CurrTotp, error) {
	if totp.Digits == 0 {
		totp.Digits = 6
	}
	if totp.Period == 0 {
		totp.Period = 30
	}
	var hasher *gotp.Hasher
	if totp.Algorithm != "" {
		var digest func() hash.Hash
		switch strings.ToLower(totp.Algorithm) {
		case "sha1", "sha-1":
			digest = sha1.New
		default:
			return nil, errors.New("unknown algorithm: " + totp.Algorithm)
		}
		hasher = &gotp.Hasher{
			totp.Algorithm,
			digest,
		}
	}

	return Totp{gotp.NewTOTP(totp.Secret, totp.Digits, int(totp.Period), hasher)}, nil
}

type CurrTotp interface {
	Current() string
}

type Totp struct {
	totp *gotp.TOTP
}

func (t Totp) Current() string {
	return t.totp.Now()
}
