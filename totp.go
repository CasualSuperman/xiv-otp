package main

import (
	"crypto/sha1"
	"encoding/json"
	"errors"
	"github.com/xlzd/gotp"
	"hash"
	"os"
	"strings"
)

func readTotpConfig() (currTotp, error) {
	file, err := os.Open("/mnt/c/Programs/square-enix-token.json")
	if err != nil {
		return nil, err
	}
	totp := new(JsonTOTP)
	dec := json.NewDecoder(file)
	err = dec.Decode(totp)
	if err != nil {
		return nil, err
	}

	if totp.Digits == 0 {
		totp.Digits = 6
	}
	if totp.Period == 0 {
		totp.Period = 30
	}
	var hasher *gotp.Hasher
	if totp.Algorithm != "" {
		var digest func() hash.Hash
		switch strings.ToLower(totp.Algorithm) {
		case "sha1", "sha-1":
			digest = sha1.New
		default:
			return nil, errors.New("unknown algorithm: " + totp.Algorithm)
		}
		hasher = &gotp.Hasher{
			totp.Algorithm,
			digest,
		}
	}

	return Totp{gotp.NewTOTP(totp.Secret, totp.Digits, totp.Period, hasher)}, nil
}

type currTotp interface {
	Current() string
}

type Totp struct {
	totp *gotp.TOTP
}

func (t Totp) Current() string {
	return t.totp.Now()
}

type JsonTOTP struct {
	Secret    string `json:"secret"`
	Issuer    string `json:"issuer"`
	Algorithm string `json:"algorithm"`
	Digits    int    `json:"digits,string"`
	Period    int    `json:"period,string"`
}
